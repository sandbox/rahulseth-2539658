<?php

/**
 * @file
 * Create video.
 */

/**
 * Form builder; Return form for deletion of nodes.
 */
function videocreator_selectinput($form, &$form_state) {
  // Display page 2 if $form_state['page_num'] == 2
  if (!empty($form_state['page_num']) && $form_state['page_num'] == 2) {
    return videocreator_selectinput_confirm_form($form, $form_state);
  }
  $form_state['page_num'] = 1;
  // Deletes nodes by node's titles.
  $form['video_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Video title'),
  );
  $form['images_file'] = array(
    '#title' => t('Image file'),
    '#type' => 'managed_file',
    '#progress_indicator' => 'bar',
		'#upload_location' => 'public://videocreator',
    '#upload_validators' => array(
      'file_validate_extensions' => array('zip'),
    ),
  );
  $form['audio_file'] = array(
    '#title' => t('Audio file'),
    '#type' => 'managed_file',
    '#progress_indicator' => 'bar',
		'#upload_location' => 'public://videocreator',
    '#upload_validators' => array(
      'file_validate_extensions' => array('zip'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Next',
    '#submit' => array('videocreator_selectinput_submit'),
  );
  return $form;
}

/**
 * Submit handler for the node deletion form.
 */
function videocreator_selectinput_submit($form, &$form_state) {
  $valid_rows = array();
  $form_state['video_title'] = $form_state['values']['video_title'];
  $image_zip_file = file_load($form_state['values']['images_file']);
  $audio_zip_file = file_load($form_state['values']['audio_file']);
  $img_zip_file_url = explode('//', $image_zip_file->uri);
	$aud_zip_file_url = explode('//', $audio_zip_file->uri);
  
	$img_zip_file_path = '/sites/default/files/' . $img_zip_file_url[1];
	$aud_zip_file_path = 'sites/default/files/' . $aud_zip_file_url[1];
	$zip = new ZipArchive;
	$img_res = $zip->open($img_zip_file_path);
  $aud_res = $zip->open($aud_zip_file_path);

  if ($img_res) {
	  videcreator_cleandir('sites/default/files/videcreator_image/');
    $zip->extractTo('sites/default/files/videcreator_image');
	}
	else {
		drupal_set_message('Unable to open image zip file', 'error');
		return;
	}
	if ($aud_res === TRUE) {
	  videcreator_cleandir('sites/default/files/videcreator_audio/');
    $zip->extractTo('sites/default/files/videcreator_audio');
	}
	else {
		drupal_set_message('Unable to open image zip file', 'error');
		return;
	}
  $zip->close();
	if (is_dir('sites/default/files/styles/videocreator_style')) {
		 videcreator_cleandir('sites/default/files/styles/videocreator_style/');
  }	
	$image_styles = array('videocreator_style', 'Thumbnail');
	$dir = "sites/default/files/videcreator_image/";
  $files = file_scan_directory($dir, '/.*\.jpg$/');
	foreach ($files as $file) {
	  foreach ($image_styles as $style_name) {
			$derivative_uri = image_style_path($style_name, $file->filename);
			$style_definition = image_style_load($style_name);
			// Create the image.
			image_style_create_derivative($style_definition, $file->uri, $derivative_uri);
	  }
	}	
  $form_state['page_num'] = 2;
  $form_state['rebuild'] = TRUE;
}

/**
 * Form builder; Return confirmation form for nodes deletion.
 */
function videocreator_selectinput_confirm_form($form, &$form_state) {
  global $base_url;
	$header['sr'] = 'Sr.';
  $header['name'] = 'Image Name';
	$header['img'] = 'Thumbnail';
	//$header[] = array('sr' => 'Serial No.', 'name' => 'Image Name', 'img' => 'Thumbnail');
  $dir = "sites/default/files/videcreator_image/";
  $files = file_scan_directory($dir, '/.*\.jpg$/');
	$sr = 1;
	foreach ($files as $file) {
		$variables= array(
      'style_name' => 'Thumbnail',// my image cache
                  'path' =>'public://' . $file->filename,
                  'alt' => $file->filename,
             );
                $img = theme('image_style', $variables);
		
	  $valid_row[] = array('sr' => $sr, 'name' => $file->filename, 'img' => $img);
		$sr++;
	}
	
  $form['msg'] = array(
    '#type' => 'markup',
    '#markup' => t('Are you sure you want to deletes these nodes ?') .
    '<br>' . t('This action cannot be undone.') . '<br>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Confirm',
    '#submit' => array('videocreator_selectinput_confirm_form'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => 'Cancel',
    '#submit' => array('videocreator_selectinput_confirm_form'),
  );
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $valid_row,
    '#empty' => t('No Valid Row(s).'),
  );
  return $form;
}

/**
 * Submit handler(Cancel) of confirmation form for nodes deletion.
 */
function videocreator_selectinput_confirm_form_submit($form, &$form_state) {
  $form_state['page_num'] = 1;
  $form_state['rebuild'] = TRUE;
}

/**
 * Implement supporting function to clean directory
 */
function videcreator_cleandir($dir) {
  $structure = glob(rtrim($dir, "/").'/*');
  if (is_array($structure)) {
    foreach($structure as $file) {
      if (is_dir($file)) 
			  videcreator_cleandir($file);
      elseif (is_file($file)) 
			  unlink($file);
    }
  }
  rmdir($dir);
}
